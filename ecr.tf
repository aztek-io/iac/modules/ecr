resource "aws_ecr_repository" "this" {
  name                 = local.name
  image_tag_mutability = var.image_tag_mutability

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = local.tags
}

resource "aws_ecr_repository_policy" "this" {
  count      = length(var.policy) > 0 ? 1 : 0
  repository = aws_ecr_repository.this.name
  policy     = var.policy
}
