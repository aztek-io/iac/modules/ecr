variable "image_tag_mutability" {
  default = "MUTABLE"
}

variable "name" {
  default = ""
}

variable "policy" {
  default = ""
}

variable "tags" {
  default = {}
}
