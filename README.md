# Module: ecr

Creates an ecr repo in AWS.

## Example Usage

How to create a lambda function using this module:

```hcl
module "ecr" {
  source = "git@gitlab.com:aztek-io/iac/modules/ecr.git?ref=v0.1.0"
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

NONE

### Optional Attributes

* `image_tag_mutability` - (Optional|string) The tag mutability setting for the repository. Must be one of: `MUTABLE` or `IMMUTABLE`. (Defaults to `MUTABLE`)
* `name`                 - (Optional|string) Name of the repository. (Defaults to a random uuid via `local.name`)
* `policy`               - (Optional|string) The policy document. This is a JSON formatted string. (Defaults to an empty string)
* `tags`                 - (Optional|map(string)) A map of tags to assign to the object. (Defaults to a map with the Name tag always set via `local.tags`)

## Attribute Reference

* `name`    - Displays the repository name.
* `tags`    - Displays the resouce tags.
